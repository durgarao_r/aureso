# == Schema Information
#
# Table name: model_types
#
#  id              :integer          not null, primary key
#  name            :string
#  model_type_code :string
#  base_price      :float
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  model_id        :integer
#  model_type_slug :string
#

require 'test_helper'

class ModelTypesControllerTest < ActionController::TestCase

  test "should get model types of model" do
    model = FactoryGirl.create(:model)

    get :index, {model_slug: model.model_slug}, format: 'json'

    assert_response :success

    json_reponse = JSON.parse(response.body)
    assert_equal json_reponse['models']['model_types'].class, Array
  end

  test "should create model type" do
    model = FactoryGirl.create(:model)

    post :create, {model_slug: model.model_slug, base_price: 150, model_type_slug: 'test123'}, format: 'json'

    assert_response :success

    json_reponse = JSON.parse(response.body)
    assert json_reponse['model_type'].key?('name')
    assert json_reponse['model_type'].key?('base_price')
    assert json_reponse['model_type'].key?('total_price')
  end
end
