# == Schema Information
#
# Table name: model_types
#
#  id              :integer          not null, primary key
#  name            :string
#  model_type_code :string
#  base_price      :float
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  model_id        :integer
#  model_type_slug :string
#

require 'test_helper'

class ModelTypeTest < ActiveSupport::TestCase


  test 'Model Type should not created without model' do
    model = FactoryGirl.build(:model_type, model: nil)
    assert_not model.save
  end

  test 'Model name should be mandatory'  do
    model = FactoryGirl.build(:model_type, name: nil)
    assert_raises(ActiveRecord::RecordInvalid) {model.save!}
  end
end
