# == Schema Information
#
# Table name: organizations
#
#  id             :integer          not null, primary key
#  name           :string
#  public_name    :string
#  type           :string
#  pricing_policy :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'test_helper'

class OrganizationTest < ActiveSupport::TestCase
  test 'Organization should not save without pricing policy' do
    organization = FactoryGirl.build(:organization, pricing_policy: nil)
    assert_not organization.save
  end

  test 'Organization name should not be more than 255 chars' do
    organization = FactoryGirl.build(:organization, name: Faker::Lorem.paragraphs)
    assert_not organization.save
  end

  test 'Organization public name should not be more than 255 chars' do
    organization = FactoryGirl.build(:organization, public_name: Faker::Lorem.paragraphs)
    assert_not organization.save
  end

  test 'Organization should save with existing' do
    organization = FactoryGirl.build(:organization)
    assert organization.save
  end
end
