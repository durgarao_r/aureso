# == Schema Information
#
# Table name: models
#
#  id              :integer          not null, primary key
#  name            :string
#  organization_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  model_slug      :string
#

require 'test_helper'

class ModelTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test 'Model should not created without organization' do
    model = FactoryGirl.build(:model, organization: nil)
    assert_not model.save
  end

  test 'Model name should be mandatory'  do
    model = FactoryGirl.build(:model, name: nil)
    assert_raises(ActiveRecord::RecordInvalid) {model.save!}
  end
end
