FactoryGirl.define do

  factory :model do
    name
    association :organization, factory: :organization
  end
end
