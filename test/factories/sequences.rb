FactoryGirl.define do
  sequence(:name) {Faker::Name.name}
  sequence(:public_name) {Faker::Name.name}
  sequence(:type) {['ShowRoom', 'Service', 'Dealer'].sample}
  sequence(:pricing_policy) {%w(Flexible Fixed Prestige).sample}
  sequence(:model_type_code) {['Show room', 'Service', 'Dealer'].sample}
  sequence(:base_price) {Faker::Number.between(1, 1000000000)}
end
