FactoryGirl.define do
  factory :model_type do
    name
    model_type_code
    base_price
    association :model, factory: :model
  end
end
