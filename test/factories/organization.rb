FactoryGirl.define do
  factory :organization do
    name
    public_name
    type
    pricing_policy
  end
end
