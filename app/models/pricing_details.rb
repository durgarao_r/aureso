class PricingDetails < ActiveHash::Base
  self.data = [
      {name: 'Flexible', info: {rss: false, word: 'a', url: 'http://reuters.com', total_price: Proc.new{|base_price, count| base_price * (count/100)}}},
      {name: 'Fixed', info: {rss: false, word: 'status', url: 'https://developer.github.com/v3/#http-redirects', total_price: Proc.new{|base_price, count| base_price + count}}},
      {name: 'Prestige', info: {rss: true, word: 'pubDate', url: 'http://www.yourlocalguardian.co.uk/sport/rugby/rss/', total_price: Proc.new{|base_price, count| base_price + count}}}
  ]
end
