# == Schema Information
#
# Table name: models
#
#  id              :integer          not null, primary key
#  name            :string
#  organization_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  model_slug      :string
#

class Model < ActiveRecord::Base
  extend FriendlyId

  friendly_id :name, use: :slugged, slug_column: :model_slug

  has_many :model_types
  belongs_to :organization

  validates_presence_of :name
  validates_presence_of :organization
end
