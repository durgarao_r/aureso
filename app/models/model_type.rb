# == Schema Information
#
# Table name: model_types
#
#  id              :integer          not null, primary key
#  name            :string
#  model_type_code :string
#  base_price      :float
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  model_id        :integer
#  model_type_slug :string
#

class ModelType < ActiveRecord::Base
  extend FriendlyId
  belongs_to :model
  attr_accessor :total_price

  friendly_id :name, use: :slugged, slug_column: :model_type_slug

  validates_presence_of :name
  validates_presence_of :model

  before_update :update_name

  def update_name
    if model_type_slug_changed?
      self.name = self.model_type_slug
    end
  end

  def total_price
    model.organization.pricing_logic(base_price)
  end
end
