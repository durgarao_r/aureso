# == Schema Information
#
# Table name: organizations
#
#  id             :integer          not null, primary key
#  name           :string
#  public_name    :string
#  type           :string
#  pricing_policy :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
require 'open-uri'

class Organization < ActiveRecord::Base
  has_many :models

  validates :pricing_policy, inclusion: {in: %w(Flexible Fixed Prestige)}
  validates :name, length: {maximum: 255}
  validates :public_name, length: {maximum: 255}

  def pricing_logic(base_price)
    pricing_details = PricingDetails.find_by_name(pricing_policy)[:info]

    words_count =
        if pricing_details[:rss]
          xml_parsing(pricing_details)
        else
          html_parsing(pricing_details)
        end

    pricing_details[:total_price].call(base_price, words_count)
  end

  def xml_parsing(pricing_details)
    body = Nokogiri::XML(open(pricing_details[:url]))
    body.search(pricing_details[:word]).size
  end

  def html_parsing(pricing_details)
    body = Nokogiri::HTML(open(pricing_details[:url])).css('body')
    body.css('script style noscript').remove
    body.text.split(pricing_details[:word]).size
  end
end
