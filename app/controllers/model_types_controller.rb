# == Schema Information
#
# Table name: model_types
#
#  id              :integer          not null, primary key
#  name            :string
#  model_type_code :string
#  base_price      :float
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  model_id        :integer
#  model_type_slug :string
#

class ModelTypesController < ApplicationController
  before_filter :get_model

  def index
    models = @model.as_json(only: :name,
                            include: {model_types: {only: :name, methods: :total_price}})

    render json: {models: models}
  end

  def create
    model_type = @model.model_types.build(model_type_params.merge(name: params[:model_type_slug]))

    if model_type.save
      render json: {model_type: model_type.as_json(only: [:name, :base_price],
                                                   methods: :total_price)}
    else
      render json: {status: 'failure'}, status: :unprocessable_entity
    end
  end

  protected
  def get_model
    @model = Model.friendly.find(params[:model_slug])
  end

  def model_type_params
    params.permit(:base_price, :model_type_slug)
  end
end
