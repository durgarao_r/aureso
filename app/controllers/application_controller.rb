class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  unless Rails.env.test?
    http_basic_authenticate_with name: 'aureso', password: 'aureso'
  end

  rescue_from Exception, :with => :error_message

  private
  def error_message(error)
    render json: {message: error.message, status: 'failure'}, status: :unprocessable_entity
  end
end
