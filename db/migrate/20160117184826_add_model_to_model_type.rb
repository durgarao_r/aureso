class AddModelToModelType < ActiveRecord::Migration
  def change
    add_column :model_types, :model_id, :integer
  end
end
