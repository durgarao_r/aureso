class CreateModelTypes < ActiveRecord::Migration
  def change
    create_table :model_types do |t|
      t.string :name
      t.string :model_type_code
      t.float :base_price

      t.timestamps null: false
    end
  end
end
