class AddModelTypeSlugToModelType < ActiveRecord::Migration
  def change
    add_column :model_types, :model_type_slug, :string
  end
end
