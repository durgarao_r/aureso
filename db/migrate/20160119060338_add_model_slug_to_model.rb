class AddModelSlugToModel < ActiveRecord::Migration
  def change
    add_column :models, :model_slug, :string
  end
end
